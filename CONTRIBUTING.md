# Development and Contribution Guide for Wayne OS Project

## What you can do in this project 
- Technical report/suggestion: For [HW compatibility](https://gitlab.com/wayne-inc/wayneos/-/blob/2add6b88608b8f701810bd98aa607a1cf5edc71a/docs/en/release/hw_compatibility_information.md) or other [Issues](https://gitlab.com/wayne-inc/wayne_os/-/issues)
- Contribution for code/document: Make a branch yourself, modify/add it, and send merge request to the project owner
- Building Wayne OS derivative: Make a branch yourself or fork this project

## Joining Wayne OS project
1) Join Gitlab: https://gitlab.com/users/sign_in
2) Go to https://gitlab.com/wayne-inc/wayne_os
3) Press _Request Access_ button that is near the project title "Wayne OS", then wait for approval
4) After approval, [Check](https://gitlab.com/wayne-inc/wayne_os/-/project_members) your [role and permission](https://docs.gitlab.com/ee/user/permissions.html)
5) By default, each project member will get *Developer* role

## Issue
- All project members can see/create [Issues](https://gitlab.com/wayne-inc/wayne_os/-/issues)
- There are Wayne OS development status, plan, direction

## Branch
- *master*: Default, protected, at least *Maintainer* is allowed to push (add/modify/delete files and commit) or merge (allow merge request)
- At least *Developer* is allowed to create branches and push or merge to those
- Avoid confusing branch name that is similar with existing branch name
- You can delete your temporary branch after merge
- If you want to protect certain branch and restrict push/merge authority, request it to the project owner in [Issues](https://gitlab.com/wayne-inc/wayne_os/-/issues)

## Code
Wayne OS is based on Chromium OS amd64-generic board. 
<br> But Wayne OS project's repository doesn't manage upstream Chromium OS source codes as it manages only modified/additional source codes.
<br> Using/Developing Wayne OS project repository requires knowledge/experience about upstream Chromium OS project.
<br> The upstream source codes and build instructions can be referred from [Chromium OS project](http://dev.chromium.org/chromium-os).
#### Directory structure of repository
The directory structure is distingushed by feature/package name.
- _cros-src_: Modified source codes of upstream Chromium OS project
- _non-cros-src_: Additional/third-party source/binary codes which are not from upstream Chromium OS project
- _docs_: Documents that refer to wayne-os.com

## Notation
#### Date & time
You can use [ISO 8601](https://en.wikipedia.org/wiki/ISO_8601) format, but below variations are also allowed and commonly used for Wayne OS and Chromium OS project.
- YYYYMMDD
- YYMMDD
- YYYYMMDD_HHMM
- YYMMDD_HHMM
#### Naming for branch/file/etc
- Avoid capital letters unless it is required by system/platform
- Avoid special characters, except minus/hyphen[-] and underscore[_]
- Relationship between objects should be expressed by minus/hyphen[-]
- Space should be replaced to underscore[_]
- For history and to distinguish similar names, [date]/[date_time]/[identification number] can be added

|category|format|example|
| --- | --- | --- |
|file/dir|[description]|known_issue.md|
|file/dir|[description]-[description]-[description]|cros-src<br>how-to<br>non-cros-src|
|branch/etc|[subject]-[description]-[date]|report-hw_compatibility-210815<br>abc_os-managed_by_abc_company-210901|
|branch/etc|[subject]-[description]-[date_time]|test1-for_first_dev-220101_1959|
|branch/etc|[subject]-[reference]-[description]-[identification number]|tony_stark-report-hw_compatibility-210815-patch-58911|
#### Contents
- Headers:
    - # Title Can Be Written With Capital Letter
    - ## Header should be start with capital letter
    - #### Small header also similar with above header
    - classify
        - detail classify
- Proper noun or name of version/reference: _write in italic font_
- Emphasis: **write in bold font**
- Code or commands: `use back-ticks`
    ```
    Or use triple back-ticks/tildes
    ```
    
