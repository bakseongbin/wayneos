Akhir-akhir ini, berhubung dari bug projek Chromium seperti yang disebutkan di https://bugs.chromium.org/p/chromium/issues/detail?id=1177214, <br>versi 3Q20 mempunyai kendala kegagalan login Google.
<br>
<br>
Kami akan memperbaiki isu tersebut di versi 1Q21 Wayne OS.
