## Isu
Sayangnya sepeti yang disebutkan oleh Google bahwa sejak 15-03-2021, mereka akan membatasi sistem Oauth & Chrome Sync di Chromium dan Chromium OS yang dibuat oleh pihak ketiga.
<br>
<br>
Sebagaimana disebutkan berarti , login dengan Google ID akan dibatasi dan browser Chromium tidak bisa sinkronisasi dengan Google ID di Wayne OS.
<br>
<br>
Hal ini tidak hanya akan berpengaruh kepada Wayne OS dikarenakan berbagai macam browser/OS/perankat lunak yang berasal dari projek open source Chromium & Chromium OS project, yang telah menyebarkan servis Google dan berkembang bersama dalam lingkungan open source. Namun, Google sepertinya memutuskan untuk mengambil bagian dari para saingan mereka.
<br>
<br>
Hal yang akan dilakukan Wayne OS
- Wayne OS tetap akan beroperasi dengan slogan (aman, ringan, gratis).
- Kami mempertimbangkan fitur yang ‘non-google’.
- Selain dari hal yang disebutkan, kami juga akan menyediakan metode ‘personal untuk Google login di Wayne OS’ sebagaimana memungkinkan.

## Referensi
https://blog.chromium.org/2021/01/limiting-private-api-availability-in.html
<br>
https://groups.google.com/a/chromium.org/g/chromium-packagers/c/SG6jnsP4pWM
<br>
https://www.omgubuntu.co.uk/2021/01/chromium-sync-google-api-removed
<br>
https://alien.slackbook.org/blog/how-to-un-google-your-chromium-browser-experience/#comments
