## JUNG Seungsu – Kepala pusat dari SW Convergence Center, Mar 2020
Kami telah menggunakan kios Wayne OS di network lounge SW Convergence Center yang terletak di Incheon selama 15 bulan. 
<br>Performa kios Wayne OS berjalan dengan lancar tanpa melambat/eror walaupun telah digunakan setiap hari untuk serving web dan kegiatan bisnis.
<br>
Menurut saya, Wayne OS adalah os web terbaik untuk digunakan di PC spek rendah/PC lama yang sulit untuk bekerja dengan baik dengan Windows.

## Dimitri – Direktur utama, pendiri dari Seeing Beyond Words, Jan 2020
Wayne OS menghemat biaya dan waktu dari yang lebih kita harapkan. OS ini memberikan stabilitas dibandingkan dari Windows, tidak memerlukan pembaruan juga sangat terjangkau! 
<br>OS ini sungguh membantu kami yang dimana adalah perusahaan kecil untuk menghemat pengeluaran yang jauh lebih besar.

