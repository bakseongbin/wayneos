## Catatan
Wayne OS mengizinkan para pengguna/pelanggan untuk mengubah BI (identitas merk: logo, nama) dari Wayne OS dan menggunakannya untuk tujuan apapun (contoh: penggunaan internal, pendistribusian, penjualan) dibawah [Wayne OS License]().

## Persiapan
- Susun fail _png_ image anda dengan merujuk ke paket [chromiumos-assets](https://gitlab.com/wayne-inc/wayneos/-/tree/master/src/platform/chromiumos-assets).
- Periksa apakah ukuran dan nama fail pixel gambar anda sesuai dengan referensi.

## Memasukkan BI anda di Wayne OS
- [login ke mode konsol](https://gitlab.com/wayne-inc/wayneos/-/blob/9d126a29a07c963e2f06b2dd20edba12b6892bcd/docs/id/tata-cara/menggunakan_shell.md).
- Hapus fail gambar yang ada di 
<br>/usr/share/chromeos-assets/images
<br>/usr/share/chromeos-assets/images_100_percent
<br>/usr/share/chromeos-assets/images_200_percent
- Masukkan fail gambar di jalur yang disebutkan di atas (melalui USB flash drive atau ssh).
- Nyalakan ulang dan periksa BI baru.
