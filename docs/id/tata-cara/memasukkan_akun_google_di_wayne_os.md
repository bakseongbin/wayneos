## Bergabung di Google Groups
Untuk memasukkan akun Google ke Wayne OS, tambahkanlah akun Google anda ke [google-browser-signin-testaccounts@chromium.org](https://groups.google.com/u/0/a/chromium.org/g/google-browser-signin-testaccounts).
<br>
Berikutnya, masukkanlah akun Google anda di Wayne OS.
<br>

## Catatan
Metode di atas adalah untuk keperluan tes dan dikelola secara resmi oleh Google.
<br>
Namun bukan berarti anda dapat menggunakan seluruh servis dari Google dikarenakan Google membatasi/mengontrol para pihak ketiga dari opensource.
<br>

## Referensi
https://wayne-os.com/googles-restriction-for-chromium-chromium-os
<br>
https://www.chromium.org/developers/how-tos/api-keys
