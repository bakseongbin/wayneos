## Catatan
Shell hanya tersedia di versi _wayne-os-dev_ dan _wayne-os-test_ dikarenakan versi _wayne-os-base_ tidak mensupport shell

## Console
Beralih ke mode konsol: `ctrl+alt+f2` 
<br>
Beralih ke mode GUI: `ctrl+alt+f1`

## Terminal
Pertama, anda perlu untuk masuk ke GUI atau masuk Mode Tamu.
<br>
Membuka terminal: `ctrl+alt+t`
<br>
Membuka shell: `crosh> shell`

## ID & PW
- versi dev
    - ID: `chronos`
    - PW of chronos/sudo: `same with hostname` (ex: _wayne-os-1q21_)
- versi tes
    - ID: `chronos`
    - PW of chronos/sudo: `test0000`
