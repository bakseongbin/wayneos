[comment]: # ()

## Catatan
Korea: [소개.md](https://gitlab.com/wayne-inc/wayneos/-/blob/8afc99add20e3d52f9942051578f6db2ae7b452f/docs/ko/%EC%86%8C%EA%B0%9C.md)
<br>Indonesia: [pengenalan.md](https://gitlab.com/wayne-inc/wayneos/-/blob/8afc99add20e3d52f9942051578f6db2ae7b452f/docs/id/pengenalan.md)

## Apa yang disebut dengan Wayne OS?
Projek open source yang berdasar dari [Chromium OS](https://en.wikipedia.org/wiki/Chromium_OS).
<br>Projek ini akan menmberikan binari, kode, API, dokumen yang berguna dalam berbagai bahasa.

## Perbandingan diantara Chromium OS (variasi turunan dari Chromium OS) dengan Wayne OS
Memanfaatkan Chromium OS tidaklah mudah karena Chromium OS didesain untuk Chromebook (Chrome OS) dan pengguna harus merancang/menyesuaikannya, namun Wayne OS telah meningkatkan kompatibilitas dari perangkat keras untuk [AMD64](https://en.wikipedia.org/wiki/X86- 64) dengan fitur-fitur tambahan untuk pengembang/pengguna.
#### Persamaan
- Aman: Didesain untuk terlindungi dari virus/peretasan.
- Ringan: Tidak melambat (Ini bukan berarti OS bekerja cepat di perangkat manapun dikarenakan kecepatan bergantung pada perangkat keras).
- Mudah: UI/UX yang mudah digunakan untuk pengguna akhir. Tidak ada pengaturan yang rumit seperti distro Linux tradisional atau Windows.
#### Perbedaan
- Gratis: Setiap orang (individu, sekolah, perusahaan, manufaktur, organisasi, dsb.) dapat mengdownload/menginstall/menggunakan/memodifikasi/mengembangkan/mendistribusikan/menjualnya di bawah persyaratan layanan.
- Dapat disesuaikan: Anda dapat mengubah identitas merek, layar pembuka, proses login, mode, dsb. sesuai dengan kebutuhan anda.
- [Improved live USB](https://gitlab.com/wayne-inc/improved-live-usb): Berbeda dengan [live USB tradisional](https://en.wikipedia.org/wiki/Live_USB), Wayne OS USB dapat bekerja sebagai tempat penyimpanan yang dapat dilepas (dan partisi OS tersembunyi) di Windows/macOS. Wayne OS versi portabel mempunyai fitur tersebut. 

## Siapa saja membutuhkan Wayne OS

## Cara memulai projek Wayne OS
[README.md](https://gitlab.com/wayne-inc/wayneos/-/blob/d33c37bfc93320068cd62dabcf2e7bb46cfea32a/README.md)
<br>[CONTRIBUTING.md](https://gitlab.com/wayne-inc/wayneos/-/blob/d33c37bfc93320068cd62dabcf2e7bb46cfea32a/CONTRIBUTING.md)

## Isu legal
#### Persyaratan layanan
Korea: [이용약관.md](https://gitlab.com/wayne-inc/wayneos/-/blob/d33c37bfc93320068cd62dabcf2e7bb46cfea32a/docs/ko/%EB%B9%84%EC%A6%88%EB%8B%88%EC%8A%A4/%EC%9D%B4%EC%9A%A9%EC%95%BD%EA%B4%80.md)
<br>Inggris: [terms_of_service.md](https://gitlab.com/wayne-inc/wayneos/-/blob/d33c37bfc93320068cd62dabcf2e7bb46cfea32a/docs/en/business/terms_of_service.md)
#### Lisensi

## Channel resmi
- Binari & Dokumen: [wayne-os.com](https://wayne-os.com)
- Open source: [gitlab.com/wayne-inc/wayneos](https://gitlab.com/wayne-inc/wayneos)
- Forum: [facebook.com/groups/wayneosgroup](https://facebook.com/groups/wayneosgroup)
- Video: [youtube.com/c/wayneos](https://youtube.com/c/wayneos)
