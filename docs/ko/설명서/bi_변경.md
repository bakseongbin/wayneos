## 노트
[웨인 OS 라이선스]()하에, 웨인OS는 유저/고객들이 BI (brand idendity: 로고, 이름)를 어떤 목적으로든 (ex: 내부사용, 배포, 판매) 변경하는 것을 허가합니다. 

## 준비
- [chromiumos-assets](https://gitlab.com/wayne-inc/wayneos/-/tree/master/src/platform/chromiumos-assets) 패키지를 참고하여 _png_ 이미지 파일을 준비하세요.
- _png_ 파일의 픽셀 사이즈와 파일명이 참고자료와 동일한 지 확인하세요.

## Putting your BI in Wayne OS
- [콘솔 모드에 로그인]()하세요.
- 다음 경로에 존재하는 파일들을 삭제하세요.
<br>/usr/share/chromeos-assets/images
<br>/usr/share/chromeos-assets/images_100_percent
<br>/usr/share/chromeos-assets/images_200_percent
- Put your image files in above path (via USB flash drive or ssh)
- 당신의 이미지 파일들을 상기 경로에 넣으세요 (USB flash drive 혹은 ssh 이용).
- 재부팅 후 새로운 BI를 확인하세요.
