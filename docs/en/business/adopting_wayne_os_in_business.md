# Adopting Wayne OS in business

## Note
This document could be changed without any notice.
<br>If you need a consultation for each of of the following steps, feel free to ask in [Wayne OS Forum](https://www.facebook.com/groups/wayneosgroup) or send an email to evelyn@wayne-inc.com.

## Step 1: Contemplate value of Chromium OS for your business
If you are reading this, we assume that you may already know about Chromium/Wayne OS.
<br>Please read [README.md](https://gitlab.com/wayne-inc/wayneos/-/blob/11739b64c77d6e970490f45248d8e7b895a886dc/README.md) and [cases_of_wayne_os_introduction.md]() to concretize your business idea/plan.

## Step 2: Test Wayne OS on your target environment
Wayne OS is recomended for testing, since the vanilla Chromium OS doesn't provide binary and HW compatibility is less compatible.
<br>In spite of the [hw_compatibility_information.md](https://gitlab.com/wayne-inc/wayneos/-/blob/11739b64c77d6e970490f45248d8e7b895a886dc/docs/en/release/hw_compatibility_information.md), you have to check it yourself specifically.
<br>Download the latest version of Wayne OS from [wayne-os.com](https://wayne-os.com) and [test]() it on the target device.

## Step 3: Compare few ways to adopt Chormium/Wayne OS
|  |  | Source Build<br><br>Get source from Chromium OS project or Wayne OS project, then build OS yourself | Binary download<br><br>Download/use/modify Wayne OS binaries yourself | ODM<br><br>Wayne Inc. makes your OS and offers CS support |
| ------ | ------ | :------: | :------: | :------: |
|**Recommended sectors**| SW solution development | O | △ | O |
||HW manufacture | △ | △ | O |
||HW wholesale/retail | X | O | O |
||Smart factory | X | O | O |
||Office| X | O | O |
|| Education | X | O | O | 
| **Your responsibilities** | Wayne OS TOS compliance | △<details><summary>Click</summary>If you use source/binary/doc that is related with Wayne OS, you are bound to Wayne OS TOS, but if you use source/binary/doc that is unrelated with Wayne OS, you are not bound to Wayne OS TOS, and you are not related with this document</details> | O | O |
|| Open source licenses compliance<details><summary>Click</summary>Ex: open source codes to the public</details> | O | X<details><summary>Click</summary>Wayne Inc. will open source code</details> | X<details><summary>Click</summary>Wayne Inc. will open source code</details> |
|| Number of devices/users per price | - | unlimited | unlimited | 
|**Your rights about OS (source, binary, documents)**| Download, Upload | - | O | O |
|| Installation | - | O | O |
|| Use | - | O | O |
|| Modification | - | O | O |
|| Expansion | - | O | O |
|| Copy | - | O | O | O |
|| Transfer, retransfer | - | O | O | O |
|| Distribution, redistribution | - | O | O | O |
|| Sale, resale | - | O | O | O |
|| Lease, rent, lend | - | O | O | O |
|| Translation, interpretation | - | O | O | O |
|| Publication, announcement | - | O | O | O |
|**Your cost**| HR | Need experts in Chromium OS, Gentoo Linux | Need a Linux developer | Need a staff | 
|| Development period | 3-12 months | - | 1 month |
|| Price | - | $0 | $499 per 6 months |
|**OS features customization**| Your business brand OS<details><summary>Click</summary>OS name, logo, splash-screen will be changed to your brand</details> | O (DIY) | O (DIY) | O |
|| OS version information<details><summary>Click</summary>In console & login screen</details> | O (DIY) | X | O |
|| Improved live USB<details><summary>Click</summary>The USB flash drive can be worked as an OS bootable device and a USB storage</details> | X | △* | O |
|| Autologin<details><summary>Click</summary>No need google account/login, automatically open user session, default homepage with custom url</details> | O (DIY) | O (DIY) | O |
|| Mode-change<details><summary>Click</summary>Ex: kiosk, full-screen, virtual-keyboard, incognito, etc.</details> | O (DIY) | O (DIY) | O |
|| Console pw customization | O (DIY) | X | O |
|**Customer service**| Release period of new OS version | - | 3-6 months | 6 months |
|| Open tech support<details><summary>Click</summary>https://gitlab.com/wayne-inc/wayneos<br>https://facebook.com/groups/wayneosgroup<br>https://youtube.com/c/wayneos</details> | O | O | O | 
|| 1:1 exclusive chatting & video call | X | X | O |
|| Source/IDE/SDK/Google-API-keys arrangement (GCP) | O (DIY) | X | O<details><summary>Click</summary>need additional GCP charge</details> |
|| OS distribution with monthly report (GCP) | O (DIY) | X | O<details><summary>Click</summary>need additional GCP charge</details> |
|**Etc.**| More requirements  | - | X | Contact Wayne Inc. for negotiation |

## Step 4: Check Terms of service
English: [terms_os_service.md]()
<br>Korean: [이용약관.md]()

## Step 5: Contract
Send an email to evelyn@wayne-inc.com.
<br>Please include your requrements (refer Step 3), and available video meeting shedule (optional).
<br>The video meeting is available in English, Korean, Indonesian, Mandarin, Hokkien.
<br>Wayne Inc. will go over the requirements and reply within a week.
