## JUNG Seungsu - Head of SW Convergence Center, Mar 2020
We have been using Wayne OS Kiosk in Incheon SW Convergence Center’s network lounge for 15 months. 
<br>Wayne OS Kiosk has been performing well without slowing-down/error despite being running everyday for web surfing and business activities.
<br>In my opinion, Wayne OS is the best web OS for low-end/old PCs which is hard to perform well with Windows.

## Dimitri – CEO, Founder of Seeing Beyond Words, Jan 202
It’s cost and time saving much more than we have ever expected.
<br>It offers more OS stability than we had from Windows, no more update needed it is very affordable! 
<br>It really helps us as a small company saving from much bigger spending.

