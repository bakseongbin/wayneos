## Joining Google Groups
In order to sign in to Wayne OS by Google account, please add your Google account to [google-browser-signin-testaccounts@chromium.org](https://groups.google.com/u/0/a/chromium.org/g/google-browser-signin-testaccounts).
<br>
Then, sign in with your Google account in Wayne OS.
<br>

## Note
Above method is for test purpose and managed by Google officially. 
<br>
However, It doesn't mean that you can use all of Google services since Google restricts/controls third-parties of open source.
<br>

## Reference 
https://wayne-os.com/googles-restriction-for-chromium-chromium-os
<br>
https://www.chromium.org/developers/how-tos/api-keys
