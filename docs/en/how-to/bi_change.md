## Note
Wayne OS allows users/customers to change BI (brand idendity: logo, name) of Wayne OS and use it for any purposes (ex: internal use, distributing, selling) under [Wayne OS License]().

## Preparation
- Arrange your _png_ image files by referring to [chromiumos-assets](https://gitlab.com/wayne-inc/wayneos/-/tree/master/src/platform/chromiumos-assets) package.
- Check whether your image files' pixel size and name are same with the reference.

## Putting your BI in Wayne OS
- [login to console mode](https://gitlab.com/wayne-inc/wayneos/-/blob/914940f3c37e7f96e551eec5cc76cd3b8c97854e/docs/en/how-to/using_shell.md).
- Remove the existing image files in 
<br>/usr/share/chromeos-assets/images
<br>/usr/share/chromeos-assets/images_100_percent
<br>/usr/share/chromeos-assets/images_200_percent
- Put your image files in above path (via USB flash drive or ssh).
- Reboot and check the new BI.
