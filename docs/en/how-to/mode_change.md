## Note
TO-DO: This document requires contributions about useful Chromium flags.
<br>Only since _wayne-os-dev-4q21_ versions support this feature.
 
## Accessing to chrome_dev.conf
- [login to console mode](https://gitlab.com/wayne-inc/wayneos/-/blob/7946d76cc3877b012fdf227895724a453f877c18/docs/en/how-to/using_shell.md).
- Type command `/usr/sbin/mode_change-wayneos` (requires sudo pw).
- Reboot OS after modify the _chrome_dev.conf_.

#### Switching on flag:
1. Select flags in the _chrome_dev.conf_. 
2. Delete the sharp (#) which is ahead of the flag (Don't remove the sharp which is ahead of explanation).
3. Add argument, if the flag requires it.
#### Switching off flag:
1. Write a sharp mark (#) ahead of the flag.

## Useful flag set
#### For kiosk
- --kiosk: UI will be locked except web browser.
- --start-fullscreen: Web browser will be opened with full screen.
- --enable-virtual-keyboard: For touch screen.
#### For public PC
- --incognito: Web browser will be started with incognito mode.
