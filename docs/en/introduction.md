[comment]: # ()

## Note
Korean: [소개.md](https://gitlab.com/wayne-inc/wayneos/-/blob/8afc99add20e3d52f9942051578f6db2ae7b452f/docs/ko/%EC%86%8C%EA%B0%9C.md)
<br>Indonesian: [pengenalan.md](https://gitlab.com/wayne-inc/wayneos/-/blob/8afc99add20e3d52f9942051578f6db2ae7b452f/docs/id/pengenalan.md)

## What is Wayne OS?
It's an open source project based on [Chromium OS](https://en.wikipedia.org/wiki/Chromium_OS).
<br>This project intends to offer useful binaries, codes, APIs, documents in multi languages.

## Comparison Chromium OS (Chromium OS derivates) with Wayne OS
Utilizing Chromium OS is not easy as it's designed for Chromebook (Chrome OS) and users have to build/customize it, however Wayne OS has enhanced HW compatibility for [AMD64](https://en.wikipedia.org/wiki/X86-64) with additional features for developer/user.
#### Common
- Safe: Designed to protect from Virus/hacking.
- Lightweight: No slowing down (It doesn't mean that the OS is fast on any devices as the speed depends on HW).
- Easy: Easy UI/UX for end-user. No complex setup as traditional Linux distros or Windows.
#### Difference
- Free: Everyone (individual, school, company, manufacturer, organization, and etc.) can download/install/use/modify/expand/distribute/sell it under Terms of service.
- Customizable: You can modify the brand identity, splash-screen, login process, mode, etc for your purpose.
- [Improved live USB](https://gitlab.com/wayne-inc/improved-live-usb): In contrast to [legacy live USB](https://en.wikipedia.org/wiki/Live_USB), Wayne OS USB works as a removable storage (and the OS partitions are hidden) in Windows/macOS. Wayne OS portable versions include this feature.

## Who need Wayne OS

## Starting Wayne OS project
[README.md](https://gitlab.com/wayne-inc/wayneos/-/blob/eef9eef2b75c5dcdb911a731ffe9b05099fb1c55/README.md)
<br>[CONTRIBUTING.md](https://gitlab.com/wayne-inc/wayneos/-/blob/d33c37bfc93320068cd62dabcf2e7bb46cfea32a/CONTRIBUTING.md)

## Legal issues
#### Terms of service
Korean: [이용약관.md](https://gitlab.com/wayne-inc/wayneos/-/blob/d33c37bfc93320068cd62dabcf2e7bb46cfea32a/docs/ko/%EB%B9%84%EC%A6%88%EB%8B%88%EC%8A%A4/%EC%9D%B4%EC%9A%A9%EC%95%BD%EA%B4%80.md)
<br>English: [terms_of_service.md](https://gitlab.com/wayne-inc/wayneos/-/blob/d33c37bfc93320068cd62dabcf2e7bb46cfea32a/docs/en/business/terms_of_service.md)
#### License

## Official channels
- Binary & Document: [wayne-os.com](https://wayne-os.com)
- Open source: [gitlab.com/wayne-inc/wayneos](https://gitlab.com/wayne-inc/wayneos)
- Forum: [facebook.com/groups/wayneosgroup](https://facebook.com/groups/wayneosgroup)
- Video: [youtube.com/c/wayneos](https://youtube.com/c/wayneos)
